
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

  //Se crea la clase privada (_) para solo crear una instancia en toda
  // la aplicación
class _MenuProvider{

  List<dynamic> options = [];

  _MenuProvider(){
    cargarData();
  }

Future<List<dynamic>> cargarData() async {
/*
rootBundle.loadString('data/menu_opts.json')
.then((data){

 Map dataMap = json.decode(data);
 print(dataMap['rutas']);
 options = dataMap['rutas'];
 
});*/

final response = await rootBundle.loadString('data/menu_opts.json');
Map dataMap = json.decode(response);
options = dataMap['rutas'];

return options;


}

}//class

  // Se crea la instancia que sera usada en toda la app
final menuProvider = new _MenuProvider();