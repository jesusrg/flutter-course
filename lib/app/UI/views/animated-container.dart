import 'dart:math';

import 'package:flutter/material.dart'; 

class AnimatedContainerView extends StatefulWidget {
  @override
  _AnimatedContainerViewState createState() => _AnimatedContainerViewState();
}

class _AnimatedContainerViewState extends State<AnimatedContainerView> {

  double _widht = 50.0;
  double _hight = 50.0;
  Color _color = Colors.blue;
  BorderRadiusGeometry _borserRadius = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () =>  _changeProperties(),

      ),
      appBar: AppBar(
        title: Text('Animated container'),
      ),
      body: (
        Center(
          child: AnimatedContainer(
            duration: Duration(milliseconds: 1000),
            curve: Curves.fastOutSlowIn,
            width: _widht,
            height: _hight,
            decoration: BoxDecoration(
              borderRadius: _borserRadius,
              color: _color
            ),          
          ),
        )
      ),
    );
  }

  void _changeProperties() {

    final random = Random();

        setState(() {
          _widht = random.nextInt(300).toDouble();
          _hight = random.nextInt(300).toDouble();
          _color = Color.fromRGBO(
            random.nextInt(255),
            random.nextInt(255),
            random.nextInt(255),
            1);
          _borserRadius = BorderRadius.circular(random.nextInt(100).toDouble());
        });
          

  }
}