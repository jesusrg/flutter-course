import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {

  final options = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Componentes Temp'),
        ),
        body: ListView(
          children: _crearItemsCorta()



        ),


      )
    );
  }

    List<Widget> _crearItems(){

      List<Widget> lista = new List<Widget>();

      for (String option in options) {

        final temWidget = ListTile(

          title: Text(option),

        );

        // Agregar datos al array normal //
        //lista.add(temWidget);
        //lista.add(Divider());

        // OPERADOR DE CASCADA //

        lista..add(temWidget)
             ..add(Divider());
      }
      return lista;
    }

    List<Widget> _crearItemsCorta(){
      
      return options.map((String item){

        return Column(
          children: <Widget>[
            ListTile(
              title: Text(item + '!'),
              subtitle: Text('Soy un subtitulo'),
                //Imagen izquierda
              leading: Icon(Icons.account_box),
                //Imagen derecha
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: (){},
            ),
            Divider()
          ],
        );


        // Se tiene que transformar a lista
      }).toList();

    }


    // ATAJOS //

    //  cmd + . --> Menu para ver opciones del widget



}//class