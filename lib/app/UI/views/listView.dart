import 'package:flutter/material.dart';

class ListViewt extends StatefulWidget {
  ListViewt({Key key}) : super(key: key);

  _ListViewtState createState() => _ListViewtState();
}

class _ListViewtState extends State<ListViewt> {

  List<int> _numbersList = List();
  int _lastItem = 0;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addImage();
    _scrollController.addListener(() => {

      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
          _addImage()
      }

      

    });
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List View'),
      ),
      body: _createList(),
    );
  }
_createList() {
  return(
    ListView.builder(
      itemCount:_numbersList.length,
      controller: _scrollController,
      itemBuilder: (BuildContext context, int index){
        final image = _numbersList[index];  
        return(
          FadeInImage(
            image: NetworkImage('http://picsum.photos/500/300/?image=$image'),
            placeholder: AssetImage('assets/jar-loading.gif'),
          )
        );

      },

    )
  );
}

_addImage(){

  for(var i=1; i<10; i++){
    _lastItem++;
    _numbersList.add(_lastItem);

  }
  setState(() {
    
  });
}

}//class
