import 'package:flutter/material.dart';

class AvatarView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar'),
        actions: <Widget>[

          Container(
            padding: EdgeInsets.all(5),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://media.wired.com/photos/5be9d68a5d7c6a7b81d79e25/master/pass/StanLee-610719480.jpg'),
              radius: 23.0,
            ),
          ),  
          Container(
            child: CircleAvatar(
              child: Text('JR'),
              backgroundColor: Colors.white,
            ),
            margin: EdgeInsets.only(right: 10),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://static01.nyt.com/images/2018/11/13/obituaries/13LEE3/13LEE3-facebookJumbo.jpg'),
          placeholder: AssetImage('assets/jar-loading.gif'),
        )
      ),

     
      
      );
  }
}