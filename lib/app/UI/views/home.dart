import 'package:componentes/app/providers/menu.dart';
import 'package:componentes/app/utils/icons.dart';
import 'package:flutter/material.dart';
  
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        //Quita la etiqueta en la app de debug
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: _list(context)
      ),
    );
  }

  Widget _list(context) {

    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context1, AsyncSnapshot<List<dynamic>> snapshot){

        return ListView(children: _createList(snapshot.data, context));
      },
    );


    /*
    menuProvider.cargarData().then((options){
      print('_list');
      print(options);
    });
    
    return ListView(
      children: _createList(),
    );
    */
  }

  List<Widget> _createList(List<dynamic> data,  context) {

    final List<Widget> options = [];

    data.forEach((widgetData){

      final widget = ListTile(
        title: Text(widgetData['texto']),
        leading: getIcon(widgetData['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: (){
          
          Navigator.pushNamed(context, widgetData['ruta']);
        },
      );

      options..add(widget)
             ..add(Divider());

    });

    return options;

  }

}//class


// CONSUMIR RECURSOS DENTRO DE LA APP //
/*

Archivo pubspec.yaml es el equivalente al package.json, se manejaran
paquetes, dependencias y recursos estaticos

1.- Arbrir pubspec.yaml e ir a la parte de assets

2.- Agregar la ruta del archivo estatico que queremos usar

3.- Crear el provider

4.- Se necesita el paquete import 'package:flutter/services.dart' para 
    realizar el consumo del json



// NAVEGACIÓN //

1.- Crear las nuevas pantallas 

2.- Se utiliza Navigator.push(context, route), recibe dos argumentos, el primero será el
    context y el segundo el nombre de la ruta a navegar












*/