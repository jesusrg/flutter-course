import 'package:flutter/material.dart';

class AlertView extends StatelessWidget {
  const AlertView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.format_align_justify),
        onPressed: () => {
          //Navigator.pop(context)
          Navigator.pushNamed(context, 'home')

        },
      ),
        body: Center(
          child: RaisedButton(
            child: Text('Mostrar alerta'),
            color: Colors.blue,
            textColor: Colors.white,
            shape: StadiumBorder(),
            onPressed: () => _showAlert(context) ,
          ),
        ),
      );
  }

  void _showAlert(context){
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => 
      AlertDialog(
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15)),
        title: Text('Titulo'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[

            Text('Este es el contenido de la alerta'),
            FlutterLogo(
              size: 100,
            )
          ],),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],



        )
      
    );
  }

}