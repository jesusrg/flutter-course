import 'package:flutter/material.dart';

class SlidersView extends StatefulWidget {
  @override
  _SlidersViewState createState() => _SlidersViewState();
}

class _SlidersViewState extends State<SlidersView> {
  double _sliderValue = 40.0;
  bool _statusCheck = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 100),
        child: Column(
          children: <Widget>[
            _createSlider(),
            _createCheckBox(),
            _createSwitch(),
            Expanded(child: _createImage()),
          ],
        )
      ),
    );
  }

  _createSlider() {

    return Slider(
        activeColor: Colors.indigo,
        label: 'Tamaño de la imagen',
        //divisions: 20,
        onChanged: (_statusCheck) ? null :
        
        (value){
          setState(() {
            _sliderValue = value;
          
          });
          print(value);
        },
        value: _sliderValue,
        min: 10,
        max: 400,
    );
  }

  Widget _createImage() {

    return Image(
        image: NetworkImage('https://s3-us-west-2.amazonaws.com/devcodepro/media/tutorials/instalacion-de-nodejs-en-ubuntu-t1.jpg'),
        width: _sliderValue,
        fit: BoxFit.contain,
    );
  }

  _createCheckBox() {

    /* return(
      Checkbox(
        value: _statusCheck,
        onChanged: (value){
          setState(() {
            _statusCheck = value;
          });
        },
        
      )
    ); */

    return(
      CheckboxListTile(
        title: Text('Bloquear slider'),
        value: _statusCheck,
        onChanged: (value){
          setState(() {
            _statusCheck = value;
          });
        },

      )
    );

  }

  _createSwitch() {
    return(
      SwitchListTile(
          activeColor: Colors.lightGreen,
          title: Text('Bloquear slider'),
          value: _statusCheck,
          onChanged: (value){
            setState(() {
              _statusCheck = value;
            });
          },

      )
    );
  }
}




 
 