import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class InputsView extends StatefulWidget {
  _InputsViewState createState() => _InputsViewState();
}

class _InputsViewState extends State<InputsView> {
  String _name = '';
  String _email = '';
  String _date = '';
  String _selectedOption = 'Volar';
  List<String> _powers = ['Volar', 'Rayos X', 'Fuerza'];

  TextEditingController _inputDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Inputs"),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          children: <Widget>[
            _createInput(),
            Divider(),
            _createEmail(),
            Divider(),
            _createPassword(),
            Divider(),
            _createDate(context),
            Divider(),
            _createDropDown(),
            Divider(),
            _createPerson(),
          ],
        ),
      ),
    );
  }

  Widget _createInput() {
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('${_name.length}/100'),
          hintText: 'hintText',
          labelText: 'labelText',
          helperText: 'helperText',
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle)),
      onChanged: (String name) {
        setState(() {
          _name = name;
        });
      },
    );
  }

  Widget _createPerson() {
    return ListTile(
      title: Text('El nombre es: $_name'),
      subtitle: Text('Email: $_email'),
      trailing: Text(_selectedOption),
    );
  }

  _createEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('${_name.length}/100'),
          hintText: 'email',
          labelText: 'email',
          helperText: 'example@example.com',
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email)),
      onChanged: (String email) {
        setState(() {
          _email = email;
        });
      },
    );
  }

  _createPassword() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('${_name.length}/100'),
          hintText: 'password',
          labelText: 'password',
          //helperText: 'example@example.com',
          suffixIcon: Icon(Icons.security),
          icon: Icon(Icons.block)),
      onChanged: (String email) {
        setState(() {
          _email = email;
        });
      },
    );
  }

  _createDate(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputDateController,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('${_name.length}/100'),
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          //helperText: 'example@example.com',
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today)),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime date = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2020),
        locale: Locale('es', 'ES'));

    if (date != null) {
      setState(() {
        _date = date.toString();
        _inputDateController.text = _date;
      });
    }
  }

  _createDropDown() {
    return (
      Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(width: 30),
        DropdownButton(
          value: _selectedOption,
          items: getOptionsDropDown(),
          onChanged: (option) {
            print(option);
            setState(() {
              _selectedOption = option;
            });
          },
        )
      ],
    ));
  }

  List<DropdownMenuItem<String>> getOptionsDropDown() {
    List<DropdownMenuItem<String>> list = List();
    _powers.forEach((power) {
      list.add(DropdownMenuItem(
        child: Text(power),
        value: power,
      ));
    });

    return list;
  }
} //class
