
import 'package:componentes/app/UI/views/alert.dart';
import 'package:componentes/app/UI/views/animated-container.dart';
import 'package:componentes/app/UI/views/avatar.dart';
import 'package:componentes/app/UI/views/home.dart';
import 'package:componentes/app/UI/views/inputs.dart';
import 'package:componentes/app/UI/views/sliders.dart';
import 'package:componentes/app/UI/views/listView.dart';
import 'package:flutter/material.dart';


Map<String, WidgetBuilder> getApplicationRoutes() {

return <String, WidgetBuilder> {
        'home'    :  (context) => Home(),
        'alert'   :  (context) => AlertView(),
        'avatar'  :  (context) => AvatarView(),
        'animatedContainer' : (context) => AnimatedContainerView(),
        'inputs' : (context) => InputsView(),
        'list' : (context) => SlidersView(),
        'listView' : (context) => ListViewt(),
        
      };
}

